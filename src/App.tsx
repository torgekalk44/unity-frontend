import * as ReactDOM from 'react-dom';
import * as React from 'react';
import { Main } from './components/Main/Main';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Survey } from './components/Survey/Survey';
import { initializeIcons } from '@uifabric/icons';
import { Eval } from './components/Eval/Eval';
import { SurveyEval } from './components/Eval/Survey/SurveyEval';
initializeIcons();

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route exact path='/Survey/eval' component={Eval}></Route>
      <Route exact path='/Survey/:surveyId' component={Survey}></Route>
      <Route exact path='/Survey/:surveyId/eval' component={SurveyEval}></Route>
      <Route path='/' component={Main}></Route>
    </Switch>
  </BrowserRouter>,
  document.getElementById('app'));