import { IAnswer } from '../../../models/IAnswer';
import { ICategory } from '../../../models/ICategory';
import { IEvaluationData } from '../../../models/IEvaluationData';

export interface ISurveyEvalState {
    answers: IAnswer[];
    attendees: number;
    categories: string[];
    isLoading: boolean;
}