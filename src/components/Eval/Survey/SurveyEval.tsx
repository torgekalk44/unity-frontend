import * as React from 'react';
import * as styles from './SurveyEval.module.scss';
import { ISurveyEvalState } from './ISurveyEval';
import { Radar } from 'react-chartjs-2';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { RouteComponentProps } from 'react-router-dom';
import { AnswerService } from '../../../services/AnswerService';
import { IEvaluationData } from '../../../models/IEvaluationData';
import { IEvaluationCategory } from '../../../models/IEvaluationCategory';
const logo = require('../../../assets/UNITY_logo.png');

export class SurveyEval extends React.Component<RouteComponentProps<any>, ISurveyEvalState> {

  private readonly _answerService: AnswerService = new AnswerService();

  constructor(props: RouteComponentProps<any>, state: ISurveyEvalState) {
    super(props);
    this.state = {
      ...state,
      isLoading: true,
      answers: [],
      categories: []
    };
  }

  public async componentDidMount(): Promise<void> {
    if (this.props.match.params.surveyId) {
      try {
        const data = (await this._answerService.getAnswers(this.props.match.params.surveyId)).data;
        const answers = data.answers;
        const attendees = data.attendees;
        this.setState({ answers, attendees });
      } catch (error) {
        console.error(error);
      } finally {
        this.setState({ isLoading: false });
      }
    }
  }

  public render(): JSX.Element {
    const options = {
      title: {
        display: false
      },
      legend: {
        display: false
      },
      scale: {
        reverse: false,
        ticks: {
          beginAtZero: true
        }
      }
    };

    {
      return (
        <>
          <div className={styles.header}>
            <img src={logo} />
          </div>
          <div className='ms-Grid' dir='ltr'>
            <div className='ms-Grid-row'>
              <div className='ms-Grid-col ms-sm12'>
                <Typography component='h1' variant='h4' gutterBottom>
                  Auswertung der Umfrage: Agilitätscheck
                </Typography>
              </div>
            </div>
            <div className='ms-Grid-row'>
              <div className='ms-Grid-col ms-sm12 ms-lg7'>
                <Card className={styles.card}>
                  <CardContent>
                    <Radar data={this.getEvalData} options={options}> </Radar>
                  </CardContent>
                </Card>
              </div>
              <div className='ms-Grid-col ms-sm12 ms-lg5'>
                <Card className={styles.card}>
                  <CardContent>
                    Anzahl der Teilnehmer: {this.state.attendees}
                  </CardContent>
                </Card>
                <Card className={styles.card}>
                  <CardContent>
                    Anzahl der beantworteten Fragen: {this.state.answers.length}
                  </CardContent>
                </Card>
              </div>
            </div>
          </div>
        </>
      );
    }
  }
  private getEvalData = (): any => {
    const evaluationData: IEvaluationData = {
      evaluationCategories: []
    };
    const { answers, categories } = this.state;
    for (const answer of answers) {
      const add: IEvaluationCategory = {};
      const category = evaluationData.evaluationCategories.find(c => c.category.id === answer.category.id);
      if (!category) {
        add.category = answer.category;
        evaluationData.evaluationCategories.push(add);
      }

      const evaluationCategory = evaluationData.evaluationCategories.find(c => c.category.id === answer.category.id);
      if (evaluationCategory) {
        if (evaluationCategory.answer) {
          evaluationCategory.answer = Number(evaluationCategory.answer) + Number(answer.answer);
        } else {
          evaluationCategory.answer = answer.answer;
        }
      }
    }
    for (const evaluationCategory of evaluationData.evaluationCategories) {
      evaluationCategory.answer = (evaluationCategory.answer / evaluationCategory.category.questionCount) / this.state.attendees;
    }
    const labels = [];
    const dataSet = [];
    for (const evaluationCategory of evaluationData.evaluationCategories) {
      labels.push(evaluationCategory.category.name);
      dataSet.push(evaluationCategory.answer);
    }
    const data = {
      labels: labels,
      datasets: [{
        data: dataSet,
        label: '',
        backgroundColor: '#1b4b7b69',
        borderColor: '#1b4b7b69',
        pointBackgroundColor: 'rgba(179,181,198,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(179,181,198,1)'
      }]
    };
    return data;
  }
}