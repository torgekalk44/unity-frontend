import * as React from 'react';
import * as styles from './Eval.module.scss';
import { IEvalState } from './IEval';
import { Radar } from 'react-chartjs-2';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Constultant } from './consultant/Consultant';
const fkorupp = require('../../assets/images/fkorupp.jpg');
const othomsen = require('../../assets/images/othomsen.jpg');
const dneumann = require('../../assets/images/dneumann.jpg');

const options = {
  title: {
    display: false
  },
  legend: {
    display: false
  },
  scale: {
    reverse: false,
    ticks: {
      beginAtZero: true
    }
  }
};

export const Eval = (props: IEvalState): JSX.Element => {
  {
    return (
      <>
        <div className='ms-Grid' dir='ltr'>
          <div className='ms-Grid-row'>
            <div className='ms-Grid-col ms-sm12 ms-lg7'>
              <Card className={styles.radarCard}>
                <CardContent>
                  <Radar data={props.data} options={options}> </Radar>
                </CardContent>
              </Card>
            </div>
            <div className='ms-Grid-col ms-sm12 ms-lg5'>
              <Card>
                <CardContent>
                  <Typography gutterBottom>
                    Vielen Dank für die Teilnahme am Agilitätsfragebogen!
                    </Typography>
                  <Typography color='textSecondary' gutterBottom>
                    Setzen Sie sich gerne mit uns in Verbindung.
                    </Typography>
                </CardContent>
              </Card>
              <Constultant name='Frederik Korupp' title='Manager' mail='frederic.korupp@unity.de' image={fkorupp} />
              <Constultant name='Ove Thomsen' title='Senior Berater' mail='ove.thomsen@unity.de' image={othomsen} />
              <Constultant name='Daniel Neumann' title='Senior Berater' mail='daniel.neumann@unity.de' image={dneumann} />
            </div>
          </div>
        </div>
      </>
    );
  }
};
