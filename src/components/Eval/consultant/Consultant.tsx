import * as React from 'react';
import * as styles from './Consultant.module.scss';
import { IConsultantState } from './IConsultant';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const options = {
  title: {
    display: true,
    text: 'Auswertung'
  },
  scale: {
    reverse: false,
    ticks: {
      beginAtZero: true
    }
  }
};

export const Constultant = (props: IConsultantState): JSX.Element => {
  return (
    <Card className={styles.card}>
      <div className={styles.details}>
        <CardContent className={styles.content}>
          <Typography className={styles.title}>
            {props.name}
          </Typography>
          <Typography color='textSecondary' gutterBottom>
            {props.title}
          </Typography>
          <Typography color='textSecondary' gutterBottom>
            <a href={'mailto:' + props.mail}>{props.mail}</a>
          </Typography>
        </CardContent>
      </div>
      <CardMedia
        className={styles.cover}
        image={props.image}
        title={props.name}
      />
    </Card>
  );
};