export interface IConsultantState {
    name: string;
    title: string;
    mail: string;
    image: string;
}