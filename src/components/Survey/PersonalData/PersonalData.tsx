import * as React from 'react';
import { IPersonalDataProps } from './IPersonalData';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import * as styles from './PersonalData.module.scss';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

export const PersonalData = (props: IPersonalDataProps): JSX.Element => {
  return (
    <div className=' ms-Grid' dir='ltr'>
      <div className='ms-Grid-row'>
        <div className='ms-Grid-col ms-sm12 ms-md6'>
          <form>
            <div className='ms-Grid-row'>
              <div className='ms-Grid-col ms-sm12'>
                <TextField
                  onBeforeChange={props.handleInputChange('name')}
                  required={true}
                  label='Name'
                />
              </div>
            </div>
            <div className='ms-Grid-row'>
              <div className='ms-Grid-col ms-sm12'>
                <TextField
                  onBeforeChange={props.handleInputChange('email')}
                  required={true}
                  label='E-Mail'
                />
              </div>
            </div>
            <div className='ms-Grid-row'>
              <div className='ms-Grid-col ms-sm12'>
                <TextField
                  onBeforeChange={props.handleInputChange('phone')}
                  required={true}
                  label='Telefon'
                />
              </div>
            </div>
            <div className='ms-Grid-row'>
              <div className='ms-Grid-col ms-sm12'>
                <TextField
                  onBeforeChange={props.handleInputChange('company')}
                  required={true}
                  label='Firma'
                />
              </div>
              <TextField
                className={styles.protection}
                onBeforeChange={props.handleInputChange('protection')}
                required={true}
                label='protection'
              />
            </div>
            <div className='ms-Grid-row'>
              <div className='ms-Grid-col ms-sm12'>
                <Button size='large' variant='contained' color='primary' onClick={props.goToEvaluation}>
                  Absenden
                <Icon className={styles.rightIcon}>send</Icon>
                </Button>
              </div>
            </div>
          </form>
        </div>
        <div className='ms-Grid-col ms-sm12 ms-md6'>
          <Paper elevation={1} className={styles.benefits}>
            <ul>
              <Typography component='li'>
                Sofortige Auswertung erhalten.
            </Typography>
              <Typography component='li'>
                Direkte Kontaktmöglichkeit
            </Typography>
              <Typography component='li'>
                Weitere Beratung durch UNITY
            </Typography>
            </ul>
          </Paper>
        </div>
      </div>
    </div >
  );
};