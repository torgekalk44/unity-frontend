import { IAttendee } from '../../../models/IAttendee';

export interface IPersonalDataProps {
  handleInputChange(name: string): (newValue: string) => void;
  goToEvaluation(): void;
}