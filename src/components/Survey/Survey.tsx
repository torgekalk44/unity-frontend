import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { ISurveyState } from './ISurvey';
import { LoadingOverlay } from '../LoadingOverlay/LoadingOverlay';
import { SurveyService } from '../../services/SurveyService';
import { Rating } from 'office-ui-fabric-react/lib/Rating';
import Button from '@material-ui/core/Button';
import { IQuestion } from '../../models/IQuestion';
import * as styles from './Survey.module.scss';
import { v4 as uuid } from 'uuid';
import { PersonalData } from './PersonalData/PersonalData';
import { ICategory } from '../../models/ICategory';
import { Eval } from '../Eval/Eval';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import MobileStepper from '@material-ui/core/MobileStepper';
import { AnswerService } from '../../services/AnswerService';
import { ISurveyAnswer } from '../../models/ISurveyAnswer';
import { IEmail } from '../../models/IEmail';
import { EmailService } from '../../services/EmailService';
const logo = require('../../assets/UNITY_logo.png');

export class Survey extends React.Component<RouteComponentProps<any>, ISurveyState> {

  private readonly _surveyService: SurveyService = new SurveyService();

  private readonly _answerService: AnswerService = new AnswerService();

  private readonly _emailService: EmailService = new EmailService();

  constructor(props: RouteComponentProps<any>, state: ISurveyState) {
    super(props);
    this.state = {
      ...state,
      isLoading: true,
      answers: [],
      page: 1
    };
  }

  public async componentDidMount(): Promise<void> {
    if (this.props.match.params.surveyId) {
      try {
        const survey = (await this._surveyService.getSurvey(this.props.match.params.surveyId)).data;
        this.setState({ survey });
      } catch (error) {
        console.error(error);
      } finally {
        this.setState({ isLoading: false });
      }
    }
  }

  public render(): JSX.Element {
    const { survey, page } = this.state;
    const category = survey && survey.categories[page - 1];
    return (
      <React.Fragment>
        <LoadingOverlay show={this.state.isLoading} text='Lade...' />
        <div className={styles.header}>
          <img src={logo} />
        </div>
        <div className={styles.surveyContainer + ' ms-Grid'} dir='ltr'>
          <div className='ms-Grid-row'>
            <div className='ms-Grid-col ms-sm12'>
              <Typography component='h1' variant='h4' gutterBottom>
                {survey ? 'Fragebogen ' + survey.name : 'Survey not found'}
              </Typography>
            </div>
          </div>
          {
            // ToDo change to switchCase
            this.state.showEvaluation ?
              <Eval data={this.getEvalData} /> :
              this.state.showPersonalPage ?
                <PersonalData
                  handleInputChange={this.onInputChange}
                  goToEvaluation={this.goToEvaluation}
                /> :
                category &&
                <>
                  <Typography component='h2' variant='h5' gutterBottom>
                    {category.name}
                  </Typography>
                  {category.questions.map((question: IQuestion): JSX.Element =>
                    <div className={styles.questionContainer + ' ms-Grid-row'} key={question.id}>
                      <Card>
                        <div className={styles.questionLabel + ' ms-Grid-col ms-sm12 ms-lg8'}>
                          <Typography>
                            {question.label}
                          </Typography>
                        </div>
                        <div className={styles.ratingContainer + ' ms-Grid-col ms-sm12 ms-lg4'}>
                          <Rating
                            min={0}
                            max={5}
                            required={true}
                            rating={!this.state.answers.find(a => a.question.id === question.id) ? 0 : this.state.answers.find(a => a.question.id === question.id).answer}
                            onChange={this.handleRatingChange(question, category)}
                          />
                        </div>
                      </Card>
                    </div>
                  )}
                  <div className='ms-Grid-row'>
                    <MobileStepper
                      className={styles.progress}
                      variant='progress'
                      steps={survey.categories.length}
                      position='static'
                      activeStep={page - 1}
                      nextButton={
                        <Button size='small' variant='contained' color='primary' onClick={page === survey.categories.length ? this.goToPersonalPage : this.nextPage}>
                          Weiter
                        </Button>
                      }
                      backButton={
                        <Button size='small' variant='contained' onClick={this.prevPage} disabled={page === 1}>
                          Zurück
                        </Button>
                      } />
                  </div>
                </>
          }
        </div>
      </React.Fragment>
    );
  }

  private onInputChange = (name: string): (newValue: string) => void => (newValue: string): void => {
    this.setState({ attendee: { ...this.state.attendee, [name]: newValue } });
  }

  private nextPage = (): void => {
    // ToDO check page if
    let { page } = this.state;
    if (this.validateStep()) {
      page++;
      this.setState({ page });
    } else {
      alert('Bitte alle Fragen beantworten');
    }
  }

  private validateStep = (): Boolean => {
    const { answers, page } = this.state;
    const category = this.state.survey.categories[page - 1];
    const answersCategory = answers.filter(r => r.category.id === category.id);
    if (category.questions.length === answersCategory.length) {
      return true;
    } else {
      return false;
    }
  }

  private prevPage = (): void => {
    let { page } = this.state;
    page--;
    this.setState({ page });
  }

  private handleRatingChange = (question: IQuestion, category: ICategory): (event: React.FocusEvent<HTMLElement>, rating: number) => void => (event: React.FocusEvent<HTMLElement>, rating: number): void => {
    const answers = [...this.state.answers];
    const ratingObj = answers.find(r => r.question.id === question.id);

    if (ratingObj) {
      ratingObj.answer = rating;
    } else {
      answers.push({
        id: uuid(),
        category: category,
        answer: rating,
        question: question
      });
    }
    this.setState({ answers, newAnswer: 1 });
  }

  private goToPersonalPage = (): void => {
    if (this.validateStep()) {
      this.setState({ showPersonalPage: true });
    } else {
      alert('Bitte alle Fragen beantworten');
    }
  }

  private onSave = async (): Promise<void> => {
    this.setState({ isLoading: true });
    try {
      const surveyAnswer: ISurveyAnswer = { answers: null, attendee: null };
      surveyAnswer.answers = this.state.answers;
      surveyAnswer.attendee = this.state.attendee;

      const id = (await this._answerService.saveAnswers(surveyAnswer)).data;
      if (id) {
        this.setState({ showEvaluation: true });
        this.sendMail();
      }
    } catch (error) {
      console.error(error);
    } finally {
      this.setState({ isLoading: false });
    }
  }

  private goToEvaluation = (): void => {
    this.onSave();
  }

  private sendMail = (): void => {
    const { survey, attendee } = this.state;
    const data: IEmail = {};
    data.name = attendee.name;
    data.email = attendee.email;
    data.phone = attendee.phone;
    data.company = attendee.company;
    data.protection = attendee.protection != null ? attendee.protection : '';
    data.survey = survey.name;
    data.surveyId = survey.id;
    this._emailService.sendMail(data);
  }

  private getEvalData = (): any => {
    const labels = [];
    const answers = [];
    for (const category of this.state.survey.categories) {
      labels.push(category.name);
      let categoryAnswers = 0;
      let answersCount = 0;
      for (const answer of this.state.answers) {
        if (answer.category.id === category.id) {
          categoryAnswers = categoryAnswers + answer.answer;
          answersCount++;
        }
      }
      answers.push(categoryAnswers / answersCount);
    }

    const data = {
      labels: labels,
      datasets: [{
        data: answers,
        label: this.state.survey.name,
        backgroundColor: '#1b4b7b69',
        borderColor: '#1b4b7b69',
        pointBackgroundColor: 'rgba(179,181,198,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(179,181,198,1)'
      }]
    };
    return data;
  }
}