import { ISurvey } from '../../models/ISurvey';
import { IAnswer } from '../../models/IAnswer';
import { IAttendee } from '../../models/IAttendee';

export interface ISurveyState {
  name: string;
  firstName: string;
  email: string;
  survey: ISurvey;
  isLoading: boolean;
  page: number;
  showPersonalPage: boolean;
  showEvaluation: boolean;
  answers: IAnswer[];
  newAnswer: number;
  attendee: IAttendee;
}