import * as React from 'react';
import { ILoadingProps } from './ILoadingOverlay';
import { Spinner, SpinnerSize } from 'office-ui-fabric-react/lib/Spinner';
import * as styles from './LoadingOverlay.module.scss';

export const LoadingOverlay = (props: ILoadingProps): JSX.Element => {
    if (!props.show) { return null; }

    return (
        <div className={styles.spinnerContainer}>
            <Spinner size={SpinnerSize.large}></Spinner>
            <div className={styles.text}>{props.text}</div>
        </div>
    );
}