export interface ILoadingProps {
    show: boolean;
    text: string;
}