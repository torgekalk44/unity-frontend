import { ISurvey } from '../../models/ISurvey';

export interface IMainState {
    surveys: ISurvey[];
    isLoading: boolean;
}