import * as React from 'react';
import * as styles from './Main.module.scss';
import { IMainState } from './IMain';
import { SurveyService } from '../../services/SurveyService';
import { ISurvey } from '../../models/ISurvey';
import Card from '@material-ui/core/Card';
import { CardContent, CardActions, Typography } from '@material-ui/core';
import { ActionButton } from 'office-ui-fabric-react/lib/Button';
const logo = require('../../assets/UNITY_logo.png');

export class Main extends React.Component<{}, IMainState> {

  private readonly _surveyService: SurveyService = new SurveyService();

  constructor(props: {}, state: IMainState) {
    super(props);
    this.state = {
      ...state,
      isLoading: true,
      surveys: []
    };
  }

  public async componentDidMount(): Promise<void> {
    try {
      const surveys = (await this._surveyService.getAllSurveys()).data;
      this.setState({ surveys });
    } catch (error) {
      console.error(error);
    } finally {
      this.setState({ isLoading: false });
    }
  }

  public render(): JSX.Element {
    return (
      <>
        <div className={styles.header}>
          <img src={logo} />
        </div>
        <div className='ms-Grid' dir='ltr'>

          <div className='ms-Grid-row'>
            {this.state.surveys.map((survey: ISurvey): JSX.Element =>
              <div className='ms-Grid-col ms-sm-12 ms-md6 ms-lg4' key={survey.id}>
                <Card className={styles.unityBtn} classes={{ root: styles.card }} key={survey.name}>
                  <CardContent>
                    <Typography className={styles.title}>
                      {survey.name}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <ActionButton data-automation-id='add' iconProps={{ iconName: 'AddFriend' }} allowDisabledFocus={true} href={'/survey/' + survey.id}>
                      Anzeigen
                        </ActionButton>
                    <ActionButton data-automation-id='add' iconProps={{ iconName: 'DynamicSMBLogo' }} allowDisabledFocus={true} href={'/survey/' + survey.id + '/eval'}>
                      Auswertung
                        </ActionButton>
                  </CardActions>
                </Card>
              </div>
            )}
          </div>
        </div>
      </>
    );
  }
}