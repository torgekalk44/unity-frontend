import { ISurvey } from '../models/ISurvey';
import axios, { AxiosPromise } from 'axios';

export class SurveyService {
    public getSurvey = (surveyId: number): AxiosPromise<ISurvey> => {
        const requestUrl = `http://localhost/rest/surveyService.php?getSurvey&surveyId=${surveyId}`;
        return axios.get(requestUrl);
    }

    public getAllSurveys = (): AxiosPromise<ISurvey[]> => {
        const requestUrl = 'http://localhost/rest/surveyService.php?getAllSurveys';
        return axios.get(requestUrl);
    }
}