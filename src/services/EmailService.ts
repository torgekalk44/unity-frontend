import { ISurvey } from '../models/ISurvey';
import axios, { AxiosPromise } from 'axios';
import { IAnswer } from '../models/IAnswer';
import { ISurveyAnswer } from '../models/ISurveyAnswer';
import { IAnswerResponse } from '../models/IAnswerResponse';
import { IEmail } from '../models/IEmail';

export class EmailService {
    public sendMail = (data: IEmail): AxiosPromise<number> => {
        const requestUrl = 'http://localhost/rest/emailService.php?sendMail';
        return axios.post(requestUrl, data);
    }
}