import { ISurvey } from '../models/ISurvey';
import axios, { AxiosPromise } from 'axios';
import { IAnswer } from '../models/IAnswer';
import { ISurveyAnswer } from '../models/ISurveyAnswer';
import { IAnswerResponse } from '../models/IAnswerResponse';

export class AnswerService {
    public saveAnswers = (answers: ISurveyAnswer): AxiosPromise<ISurvey> => {
        const requestUrl = 'http://localhost/rest/answerService.php?postAnswers';
        return axios.post(requestUrl, answers);
    }
    public getAnswers = (surveyId: number): AxiosPromise<IAnswerResponse> => {
        const requestUrl = `http://localhost/rest/answerService.php?getAnswers&surveyId=${surveyId}`;
        return axios.get(requestUrl);
    }
}