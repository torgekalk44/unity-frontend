import { ICategory } from './ICategory';

export interface ISurvey {
  id: number;
  name: string;
  categories: ICategory[];
}