import { ISurvey } from './ISurvey';

export interface IEmail {
    name?: string;
    email?: string;
    phone?: string;
    company?: string;
    protection?: string;
    surveyId?: number;
    survey?: string;
}