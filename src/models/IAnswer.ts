import { IQuestion } from './IQuestion';
import { ICategory } from './ICategory';

export interface IAnswer {
  id: number | string;
  category: ICategory;
  answer?: number;
  question: IQuestion;
}