import { IAnswer } from './IAnswer';
import { IAttendee } from './IAttendee';
import { IEvaluationCategory } from './IEvaluationCategory';

export interface IEvaluationData {
    answers?: IAnswer[];
    attendee?: IAttendee;
    evaluationCategories?: IEvaluationCategory[];
}