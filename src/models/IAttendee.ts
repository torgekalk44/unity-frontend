export interface IAttendee {
  id: number | string;
  name: string;
  email: string;
  phone: string;
  company: string;
  protection: string;
}