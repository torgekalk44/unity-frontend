import { IAnswer } from './IAnswer';
import { IAttendee } from './IAttendee';

export interface ISurveyAnswer {
  answers: IAnswer[];
  attendee: IAttendee;
}