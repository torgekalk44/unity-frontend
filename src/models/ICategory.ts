import { IQuestion } from './IQuestion';

export interface ICategory {
  id: number;
  name: string;
  surveyId: number;
  questions: IQuestion[];
  questionCount?: number;
}