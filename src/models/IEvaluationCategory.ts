import { ICategory } from './ICategory';

export interface IEvaluationCategory {
    category?: ICategory;
    answer?: number;
}