export interface IQuestion {
    id: number;
    label: string;
    description: string;
    questionOrder: number;
}