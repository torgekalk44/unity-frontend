import { IQuestion } from './IQuestion';
import { ICategory } from './ICategory';
import { IAnswer } from './IAnswer';

export interface IAnswerResponse {
    attendees: number;
    answers: IAnswer[];
}